import { Component, NgZone } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import firebase from 'firebase'



import { HomePage } from '../pages/home/home';
import { Login } from '../pages/login/login';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = HomePage;
  zone: NgZone;
  constructor(private _ngZone: NgZone, platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    this.zone = new NgZone({});


    firebase.initializeApp({

      apiKey: "AIzaSyAxZjXAeSx5TBUFZHl0dKCg9zvQ1a6Dsqg",
      authDomain: "eventtutorial-ed9f0.firebaseapp.com",
      databaseURL: "https://eventtutorial-ed9f0.firebaseio.com",
      projectId: "eventtutorial-ed9f0",
      storageBucket: "eventtutorial-ed9f0.appspot.com",
      messagingSenderId: "570622710483"



    });


    const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
      this.zone.run(() => {

        if (!user) {
          this.rootPage = Login;
          unsubscribe();

        } else {

          this.rootPage = HomePage
          unsubscribe();
        }
      });

    });



    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

