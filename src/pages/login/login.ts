import { Component } from '@angular/core';
import { IonicPage, NavController, Loading, LoadingController, AlertController, NavParams } from 'ionic-angular';


import { FormBuilder, Validators, FormGroup } from '@angular/forms';

import { AuthData } from '../../providers/auth-data'

import { HomePage } from '../home/home'
import { Signup } from '../signup/signup'
//import { EmailValidator } from '../../validators/email';

/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {
  public loginForm: FormGroup;
  public loading: Loading;
  public signup: any = Signup;
  constructor(public loadingCtrl: LoadingController,public alertCtrl: AlertController,  public authProvider: AuthData, public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder) {

    this.loginForm = formBuilder.group({
      email: [''],
      password: ['']

    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
  }

  loginUser(): void {
    if (!this.loginForm.valid) {
      console.log(this.loginForm.value);
    } else {
      this.authProvider.loginUser(this.loginForm.value.email,
        this.loginForm.value.password)
        .then(authData => {
          this.loading.dismiss().then(() => {
            this.navCtrl.setRoot(HomePage);
          });
        }, error => {
          this.loading.dismiss().then(() => {
            let alert = this.alertCtrl.create({
              message: error.message,
              buttons: [
                {
                  text: "Ok",

                  role: 'cancel'
                }
              ]
            });
            alert.present();
          });
        });
      this.loading = this.loadingCtrl.create();
      this.loading.present();
    }
  }

  stuff() {

    alert("This is stuff");
  }
  goToSignup(): void { this.navCtrl.push(this.signup) }
  goToResetPassword(): void { this.navCtrl.push('reset-password') }
}
