import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams } from 'ionic-angular';

import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AuthData } from '../../providers/auth-data'
/**
 * Generated class for the ResetPassword page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-reset-password',
  templateUrl: 'reset-password.html',
})
export class ResetPassword {

  public resetPasswordForm: FormGroup;
  constructor(public authProvider: AuthData, public alertCtrl: AlertController, public formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams) {

    this.resetPasswordForm = formBuilder.group({

      email: ['']


    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetPassword');
  }

  resetPassword() {


    this.authProvider.resetPassword(this.resetPasswordForm.value.email)
      .then((user) => {
        let alert = this.alertCtrl.create({
          message: "We just sent you a reset link to your email",
          buttons: [
            {
              text: "Ok",
              role: 'cancel',
              handler: () => { this.navCtrl.pop(); }
            }
          ]
        });
        alert.present();
      })



  }
      

}
