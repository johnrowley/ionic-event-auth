import { Component } from '@angular/core';
import { IonicPage, NavController, Loading, LoadingController, AlertController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HomePage } from '../home/home'
import { AuthData } from '../../providers/auth-data'

/**
 * Generated class for the Signup page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class Signup {
  public signupForm: FormGroup;
  loading: Loading;
  constructor(public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController, public authProvider: AuthData,
    public navCtrl: NavController, public navParams: NavParams) {

    this.signupForm = formBuilder.group({
      email: [''],
      password: ['']
    });



  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Signup');
  }


  signupUser() {
    if (!this.signupForm.valid) {
      console.log(this.signupForm.value);
    } else {
      this.authProvider.signupUser(this.signupForm.value.email,
        this.signupForm.value.password)
        .then(() => {
          this.loading.dismiss().then(() => {
           
            this.navCtrl.setRoot(HomePage);
          });
        }, (error) => {
          this.loading.dismiss().then(() => {
            let alert = this.alertCtrl.create({
              message: error.message,
              buttons: [
                {
                  text: "Ok",
                  role: 'cancel'
                }
              ]
            });
            alert.present();
          });
        });
      this.loading = this.loadingCtrl.create();
      this.loading.present();
    }
  }
}
